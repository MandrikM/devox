import React, {Component} from 'react'
import img3 from "../images/image3.png"
import image from "../images/image.png"
import Header from "./Header";

class Banner extends Component {
    render() {
        return (
            <div className="Banner">
                <Header/>
                <div className="BannerMain">
                    <div>
                        <div className="AirDe">Air De MUsk</div>
                        <div className="AllTheCollection"> All the new collection</div>
                        <button className="BannerBut">Shop now</button>
                    </div>
                    <div className="">
                        <img src={img3} alt="Img3" style={{
                            width: "519.84 px",
                            height: "686px", position: "relative", top: 0, left: 0
                        }}/>
                        <img src={image} alt="Ima" style={{
                            width: "247.29px",
                            height: "238.02px", position: "relative", bottom: 80, right: 200
                        }}/>
                    </div>
                </div>
            </div>
        )
    }
}

export default Banner
