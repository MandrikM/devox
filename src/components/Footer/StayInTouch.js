import React, {Component} from 'react'

class StayInTouch extends Component {
    render() {
        return (
            <div>
                <h3>Stay in touch </h3>
                <p> Receive news about iOud’s latest products and updates</p>
                <p> Email</p>
                <hr/>
                <button style={{
                    border: "1px solid #FFFFFF",
                    "box-sizing": "border-box", background: "none", color: "white", width: "116px",
                    height: "50px"
                }}>Save
                </button>
            </div>
        )
    }
}

export default StayInTouch
