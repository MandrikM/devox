import React, {Component} from 'react'
import fb from "../../images/fb.png"
import inst from "../../images/inst.png"
import twit from "../../images/twit.png"

class SosialIcons extends Component {
    render() {
        return (
            <div className="Icons">
                <img src={fb}/>
                <img src={inst}/>
                <img src={twit}/>

            </div>
        )
    }
}

export default SosialIcons
