import React, {Component} from 'react'
import Advantages from "./Advantages";
import Banner from "./Banner"
import Footer from "./Footer";


class Main extends Component {
    render() {
        return (
            <div className="Main">

                {/**/}

                <Banner/>

                <Advantages/>
                <div className="Empty"/>
                <Footer/>
            </div>
        )
    }
}

export default Main
