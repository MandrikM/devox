import React, {Component} from 'react'
import Legal from "./Footer/Legal";
import IQud from "./Footer/IQud";
import StayInTouch from "./Footer/StayInTouch";

class Footer extends Component {
    render() {
        return (

            <div className="Footer">
                <div className="Footerdiv">
                    <Legal/>
                    <IQud/>
                    <StayInTouch/>
                </div>
                <p className="Footerdiv"> ©2019 all rights reserved to Intelligent Oud Co. for Trading</p>
            </div>


        )
    }
}

export default Footer
