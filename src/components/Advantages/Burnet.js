import React, {Component} from 'react'
import bur from "../../images/burnet.png"

class Burnet extends Component {
    render() {
        return (
            <div style={{background: " #FFFAF6"}} className="Rectangular">
                <img src={bur} style={{margin: "20px 0"}}/>
                <div> No Burner needed</div>

            </div>
        )
    }
}

export default Burnet
