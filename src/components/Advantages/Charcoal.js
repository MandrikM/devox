import React, {Component} from 'react'
import charcoal from "../../images/charcoal.png"

class Charcoal extends Component {
    render() {
        return (
            <div style={{background: " #EFEAE6"}} className="Rectangular">

                <img src={charcoal} style={{margin: "20px 0"}}/>
                <div> No Charcoala needed</div>
            </div>
        )
    }
}

export default Charcoal
