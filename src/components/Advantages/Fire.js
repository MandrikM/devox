import React, {Component} from 'react'
import fire from "../../images/fire.png"

class Fire extends Component {
    render() {
        return (
            <div style={{background: " #EFEAE6"}} className="Rectangular">
                <img src={fire} style={{margin: "20px 0"}}/>
                <div> No Fire needed</div>

            </div>
        )
    }
}

export default Fire
