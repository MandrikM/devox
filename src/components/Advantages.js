import React, {Component} from 'react'
import Charcoal from "./Advantages/Charcoal";
import Burnet from "./Advantages/Burnet";
import Fire from "./Advantages/Fire";

class Advantages extends Component {
    render() {
        return (
            <div className="Advantages">

                <Charcoal/>
                <Burnet/>
                <Fire/>

            </div>
        )
    }
}

export default Advantages
